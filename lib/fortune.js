const fortuneCookies = [
    "Fortune 1",
    "Fortune 2",
    "Fortune 3",
    "Fortune 4",
    "Fortune 5"
]

exports.getFortune = () => {
    const idx = Math.floor(Math.random() * fortuneCookies.length)
    return fortuneCookies[idx]
}