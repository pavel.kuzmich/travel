const fortune = require('./fortune')

exports.home = (req, res) => res.render('home')

exports.about = ((req, res) => {
    res.render('about', {fortune: fortune.getFortune()})
})

exports.notFound = (req, res) => res.render('404')

/* eslint-disable no-unused-vars */
exports.serverError = (err, req, res, next) => res.render('500')
/* eslint-enable no-unused-vars */


/**
 * Fetch JSON
 */
exports.newsletter = (req, res) => {
    res.render('newsletter', { csrf: 'Theres is token CSRF' })
}

exports.api = {
    newsletterSignup: (req, res) => {
        console.log('Token CSRF: ' + req.body._csrf)
        console.log('Name: ' + req.body.name)
        console.log('E-mail: ' + req.body.email)
    
        res.send({ result: 'success' })
    },
}
