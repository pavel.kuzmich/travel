// 1. Импортируем код, который нужно протестировать
const handlers = require('../handlers')

test('Home page renders', () => {
    const req = {}                      // объект запроса
    const res = { render: jest.fn() }   // объект ответа

    // тестируем, что домашняя страница отображается
    handlers.home(req, res)

    expect(res.render.mock.calls[0][0]).toBe('home') // первый индекс - номер вызова; второй - номер аргумента
})

test('About page renders', () => {
    const req = {}
    const res = { render: jest.fn() }

    handlers.about(req, res)

    expect(res.render.mock.calls.length).toBe(1)
    expect(res.render.mock.calls[0][0]).toBe('about')
    expect(res.render.mock.calls[0][1])
        .toEqual(expect.objectContaining({
            fortune: expect.stringMatching(/\W/),
        }))
})

test('404 page rendes', () => {
    const req = {}
    const res = { render: jest.fn() }
    
    handlers.notFound(req, res)

    expect(res.render.mock.calls.length).toBe(1)
    expect(res.render.mock.calls[0][0]).toBe('404') 
})

test('500 page rendes', () => {
    const err = new Error('Some error')
    const req = {}
    const res = { render: jest.fn() }
    const next = jest.fn()
    
    handlers.serverError(err, req, res, next)

    expect(res.render.mock.calls.length).toBe(1)
    expect(res.render.mock.calls[0][0]).toBe('500') 
})