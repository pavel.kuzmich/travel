const express = require('express')
const expressHandlebars = require('express-handlebars')

// Для обработки форм, с помощью Express
const bodyParser = require('body-parser')

// Обработчик маршрутов
const handlers = require('./lib/handlers')

const app = express()
const port = process.env.PORT || 3000

// инициализация Handlebars
app.engine('handlebars', expressHandlebars({
    defaultLayout: 'main'
}))
app.set('view engine', 'handlebars')

/**
 * Добавление промежуточного ПО
 */
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

/**
 * Home page
 */
app.get('/', handlers.home)


/**
 * handlers for Newsletter Form
 */
app.get('/newsletter', handlers.newsletter)
//app.post('/api/newsletter-signup', handlers.api.newsletterSignup)

// About page
app.get('/about', handlers.about)

// 404 Page
app.use(handlers.notFound)

// 500 Page
app.use(handlers.serverError)

if(require.main === module) {
    app.listen(port, () => console.log(`Express started on htpp://localhost:${port}`))
} else {
    module.exports = app
}