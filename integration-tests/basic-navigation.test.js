const portfinder = require('portfinder')
const puppeteer = require('puppeteer')

const app = require('../meadowlark')

let server = null
let port = null

// 1. Запукает сервер на незанятом порте
beforeEach(async () => {
    port = await portfinder.getPortPromise()
    server = app.listen(port)
})

afterEach(() => {
    server.close()
})

test('Homepage link in About page', async () => {
    // 2. Запускает браузер в режиме headless
    const browser = await puppeteer.launch()

    // 3. Переходит на страницу Homepage
    const page = await browser.newPage()
    await page.goto(`http://localhost:${port}`)

    // 4. Находит ссылку с атрибутом data-test-id="about" и щёлкает по ней
    await Promise.all([
        page.waitForNavigation(),
        page.click('[data-test-id="about"]'),
    ])

    // 5. Ожидает перехода по ссылке
    
    // 6. Проверяет, что мы находимся на странице About
    expect(page.url()).toBe(`ttp://localhost:${port}/about`)

    await browser.close()
})