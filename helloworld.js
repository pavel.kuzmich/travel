const http = require('http')
const fs = require('fs')
const port = process.env.PORT || 3000

function serverStatiFile(res, path, contentType, responseCode = 200) {
    fs.readFile(__dirname + path, (err, data) => {
        if(err) {
            res.writeHead(500, {'Content-Type': 'text/plain'})
            return res.end('Error 500')
        }
        res.writeHead(responseCode, {'Content-Type': contentType})
        res.end(data)
    })
}

const server = http.createServer((req, res) => {
    const path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase()

    switch(path) {
        case '':
            serverStatiFile(res, '/public/home.html', 'text/plain')
            break
        case '/about':
            serverStatiFile(res, '/public/about.html', 'text/plain')
            break
        case '/public/img/logo.png':
            serverStatiFile(res, '/public/img/logo.png', 'image/png')
            break
        default:
            serverStatiFile(res, '/public/404.html', 'text/plain', 404)
            break
    }
})

server.listen(port, () => console.log(`server is going on port ${port}`))